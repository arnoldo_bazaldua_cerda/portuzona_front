(function () {
  'use strict';

  /**
  * Signup Module
  *
  * Description
  */
  angular.module('ptzApp')

    .controller('SignupCtrl', ['User', '$mdDialog', SignupCtrl]);

    function SignupCtrl (User, $mdDialog) {
      var vm = this;
      vm.user = {};
      vm.save = save;

      function save () {
        User.create(angular.copy(vm.user)).success(function (data) {
         console.log(data);
        });
      }
    }
})();