/**
* @ngdoc function
* @name ptzApp.controller:MainCtrl
* @description
* # MainCtrl
* Controller of the ptzApp
*/

(function () {
  'use strict';

  angular
    .module('ptzApp')
    .controller('MainCtrl', MainCtrl);

    function MainCtrl ($mdBottomSheet, uiGmapGoogleMapApi, uiGmapIsReady, Categories) {

      var vm = this;
      vm.markerClick = markerClick;
      vm.location = {};
      vm.markers = [];
      vm.rndMarkers = [];
      vm.selectItem = selectItem;

      uiGmapGoogleMapApi.then(activate);

      /*if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
          vm.location.id = 6;
          vm.location.coords = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
          };
          console.log(vm.location.coords);
        }, function () {
          console.log('permission not granted!');
        });
      }*/

      function activate (maps) {
        vm.map = { control: {}, center: { latitude: 25.686776, longitude: -100.316667 }, zoom: 14 };
        vm.location = {id: 6, coords: { latitude: 25.686776, longitude: -100.316667 }};
        var directionsService = new maps.DirectionsService;
        var directionsDisplay = new maps.DirectionsRenderer;

        Categories.all().success(function (data) {
          vm.categories = data;
        });
      }
          
      function markerClick (map, event, model) {
        $mdBottomSheet.show({
          templateUrl: 'views/marker-info.html',
          targetEvent: map,
          parent: '#testing',
          locals: {location: vm.location, marker: model, map: vm.map.control.getGMap()},
          controller: 'markerInfoCtrl',
          controllerAs: 'marker'
        });
      }

      function selectItem (item) {
        if (item.isSelected) {
          vm.rndMarkers = [
            {id: 1, coords: {latitude: 25.692925, longitude: -100.309620}},
            {id: 2, coords: {latitude: 25.691800, longitude: -100.323116}},
            {id: 3, coords: {latitude: 25.682179, longitude: -100.328883}},
            {id: 4, coords: {latitude: 25.695874, longitude: -100.299034}},
            {id: 5, coords: {latitude: 25.677808, longitude: -100.299791}}
          ];
        }
      }
    }
})();