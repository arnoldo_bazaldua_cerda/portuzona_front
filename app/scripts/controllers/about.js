'use strict';

/**
 * @ngdoc function
 * @name ptzApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the ptzApp
 */
angular.module('ptzApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
