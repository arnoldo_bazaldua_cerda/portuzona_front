(function () {
  'use strict';

  angular.module('ptzApp')
    .controller('markerInfoCtrl', ['locals', markerInfoCtrl]);

    function markerInfoCtrl (locals) {
      var vm = this;
      vm.locals = locals;
      vm.getDirections = getDirections;

      function getDirections () {
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        // console.log('getting directions...' , locals.location, locals.marker, locals.map);
        // return;
        directionsDisplay.setMap(locals.map);

        directionsService.route({
          origin: new google.maps.LatLng(locals.location.coords.latitude, locals.location.coords.longitude),
          destination: new google.maps.LatLng(locals.marker.coords.latitude, locals.marker.coords.longitude),
          travelMode: google.maps.TravelMode.DRIVING
        }, function(response, status) {
          if (status === google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
      } 
    }
})();