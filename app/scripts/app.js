/**
 * @ngdoc overview
 * @name ptzApp
 * @description
 * # ptzApp
 *
 * Main module of the application.
 */

(function () {
  'use strict';

  angular
  .module('ptzApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ui.router',
    'ngSanitize',
    'ngTouch',
    'uiGmapgoogle-maps',
    'ngMaterial',
    'ngMessages'
  ])
  .config(function ($stateProvider, $urlRouterProvider, uiGmapGoogleMapApiProvider, $mdThemingProvider) {
    $mdThemingProvider.theme('default')
      .primaryPalette('teal')
      .accentPalette('green')
      .warnPalette('deep-orange');

    uiGmapGoogleMapApiProvider.configure({
      key: 'AIzaSyCgakdIn76iZ5SzqI4V7AzyY55j4WiC5U0',
      v: '3.20',
      libraries: 'weather,geometry,visualization'
    });

    $urlRouterProvider.otherwise('/');

    $stateProvider
      // .state('home')
      .state('main', {
        url: '/',
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .state('login', {
        url: '/login',
        templateUrl: 'views/login.html',
        controller: 'AuthCtrl',
        controllerAs: 'auth'
      })
      .state('singup', {
        url: '/signup',
        templateUrl: '/views/signup.html',
        controller: 'SignupCtrl',
        controllerAs: 'signup'
      });
  })
  .constant('myConfig', {
    api: 'entuzona-1/'
  });
})();