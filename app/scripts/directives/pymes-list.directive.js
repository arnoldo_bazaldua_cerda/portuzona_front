(function () {
  'use strict';

  /**
  * ptzApp Module
  *
  * Description
  */
  angular.module('ptzApp')
    .directive('pymesList', [pymesList]);

    function pymesList () {
      return {
        restrict: 'E',
        templateUrl: 'views/pymes-list.directive.html',
        bindToController: true,
        scope: {
          collection: '='
        },
        controller: function () {
          var vm = this;

          vm.selectItem = selectItem;
          
          function selectItem (item) {
            if (item.isSelected) {
              console.log(item);
            }
          }
        },
        controllerAs: 'listCtrl'
      };
    }
})();