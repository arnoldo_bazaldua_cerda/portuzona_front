(function () {
  'use strict';

  /**
  * Users Service Module
  *
  * Description
  */
  angular.module('ptzApp')
    .factory('User', ['$http', 'myConfig', '$filter', User]);

  function User ($http, myConfig, $filter) {
    var userFactory = {};
    var api = myConfig.api;

    // get a single user
    userFactory.get = function(id) {
      return $http.get(api + 'users/' + id);
    };

    // get all users
    userFactory.all = function() {
      return $http.get(api + 'users/');
    };

    // create a user
    userFactory.create = function(userData) {
      delete userData.confirmPassword;
      userData.dateOfBirth = $filter('date')(userData.dateOfBirth, 'yyyy/MM/dd');
      userData.status = '1';
      return $http.post(api + 'users/', userData);
    };

    // update a user
    userFactory.update = function(id, userData) {
      return $http.put(api + 'users/' + id, userData);
    };

    // delete a user
    userFactory.delete = function(id) {
      return $http.delete(api + 'users/' + id);
    };
  
    return userFactory;
  }
})();