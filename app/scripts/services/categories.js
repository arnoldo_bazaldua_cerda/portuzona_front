(function () {
  'use strict';

  /**
  * Categories Service Module
  *
  * Description
  */
  angular.module('ptzApp')
    .factory('Categories', ['$http', 'myConfig', '$filter', Categories]);

  function Categories ($http, myConfig, $filter) {
    var userFactory = {};
    var api = myConfig.api;

    // get a single user
    userFactory.get = function(id) {
      return $http.get(api + 'categories/' + id);
    };

    // get all categories
    userFactory.all = function() {
      return $http.get(api + 'categories/');
    };

    // create a user
    userFactory.create = function(data) {
      return $http.post(api + 'categories/', data);
    };

    // update a user
    userFactory.update = function(id, data) {
      return $http.put(api + 'categories/' + id, data);
    };

    // delete a user
    userFactory.delete = function(id) {
      return $http.delete(api + 'categories/' + id);
    };
  
    return userFactory;
  }
})();